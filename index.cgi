#!/usr/bin/perl
use strict;
use warnings;
no warnings 'numeric';
no warnings 'uninitialized';

our (%access, %config, %text, %in);
our $module_name;

require './virtualmin-tikimanager-lib.pl';
&foreign_require("cron");
&ReadParse();

my $d;
if ($in{'dom'}) {
  $d = &virtual_server::get_domain($in{'dom'});
}

# Security check
&error_setup($text{'index_manual_err'});
$d || &error($virtual_server::text{'edit_egone'});
&virtual_server::can_edit_domain($d) ||
    &error($virtual_server::text{'edit_ecannot'});
&virtual_server::domain_has_website($d) && $d->{'dir'} ||
    &error($text{'index_nodir'});

# Protocol may be conditional
my $proto = "https://";
$proto = "http://"
  if (!&virtual_server::domain_has_ssl($d));
# Make link look nice
my $header_link = &ui_link("$proto$d->{'dom'}", &virtual_server::domain_in($d), undef,
    " target=\"_blank\"");
# Separate text and domain name
$header_link =~ s/>(?<T>.*?)(<.*?>.*?<\/.*?>)</>$2</;
$header_link = "$+{T}$header_link";

# Page title, must be first UI thing
&ui_print_header($header_link, $text{'index_title'}, "", undef, 1, 1);

#my @tiki_branches = [ map {[ $_, $_ ]} &tikimanager_get_installable_branches($d) ];
my ($tiki_branches_ref, $branches_warn, $branches_phpversion, $branches_type, $has_branches) = &tikimanager_get_installable_branches($d);
my @tiki_branches = @$tiki_branches_ref;
my $latest_branch;
$latest_branch = $tiki_branches[@tiki_branches - 1] if @tiki_branches;
my ($info) = &tikimanager_tiki_info($d);

my $phpversion = &virtual_server::get_domain_php_version($d);
$phpversion ||= &virtual_server::get_apache_mod_php_version();
$phpversion = &virtual_server::get_php_version($phpversion) || $phpversion;

my @instances = &tikimanager_tiki_list_cloneables($d);
my @cloneables;
my @upgradeables;
foreach my $instance (@instances) {
  if (&tikimanager_is_branch_installable($instance->{'branch'}, $phpversion)) {
    push(@cloneables, [
      $instance->{'instance_id'},
      "$instance->{'name'} ($instance->{'branch'})"
    ]);
  }
  push(@upgradeables, [
    $instance->{'instance_id'},
    "$instance->{'name'} ($instance->{'branch'})"
  ]);
}
my $count_cloneables = @cloneables;
my $count_branch = @tiki_branches;
my $min_support_php_version = '7.2';

# Define tabs
my $is__actions = $info && $info->{'instance_id'};
my $is__tiki_installed = tikimanager_tiki_is_installed($d);
my $is__cloneables = @upgradeables;
my $is__instance_id = $info && $info->{'instance_id'};

my @jobs;
@jobs= &cron::list_cron_jobs()
  if ($is__instance_id);

my $page = "index.cgi?dom=$d->{'dom'}&mode=";
my @tabs = ( );
my @tabnames = ( );
if ($is__actions) {
  push(@tabs, ([ "actions", "Instance Actions", $page."actions" ]));
  push(@tabnames, ("actions"));
}
if ($is__actions && $is__tiki_installed) {
  push(@tabs, ([ "instanceupdate", "Instance Update", $page."instanceupdate" ]));
  push(@tabnames, ("instanceupdate"));
}
if (!$is__actions && $is__tiki_installed) {
  push(@tabs, ([ "importinstance", "Import Existing Instance", $page."importinstance" ]));
  push(@tabnames, ("importinstance"));
}
if ((!$is__actions || !$is__tiki_installed ) && @tiki_branches ) {
  push(@tabs, ([ "installnew", "Install a New Instance", $page."installnew" ]));
  push(@tabnames, ("installnew"));
}
if ($is__cloneables) {
  push(@tabs, ([ "cloneables", "Clone", $page."cloneables" ]));
  push(@tabnames, ("cloneables"));
}
if ($is__instance_id && @jobs) {
  push(@tabs, ([ "cjobs", "Instance Jobs", $page."cjobs" ]));
  push(@tabnames, ("cjobs"));
}

if (!$is__actions && $phpversion < $min_support_php_version && (!$has_branches)) {
  print &ui_table_start("Information by Tiki Manager", "width=100%", 1);
  print &ui_table_row('Warning',&text("index_missing_install_tiki_warning", "$min_support_php_version"), 1);
  print &ui_table_end();
  print &ui_hr();
}

if ($is__actions) {
  #
  # When Tiki is installed by Tiki Manager, show Tiki info
  #

  print &ui_table_start("Information by Tiki Manager", "width=100%", 2);
  print &ui_table_row($text{'tikimanager_instance_id'}, $info->{'instance_id'}, 2);
  print &ui_table_row($text{'tikimanager_webroot'},     $info->{'webroot'}, 2);
  print &ui_table_row($text{'tikimanager_tempdir'},     $info->{'tempdir'}, 2);
  print &ui_table_row($text{'tikimanager_phpexec'},     $info->{'phpexec'}, 2);
  print &ui_table_row($text{'tikimanager_type'},        $info->{'type'}, 2);
  print &ui_table_row($text{'tikimanager_branch'},      $info->{'branch'}, 2);
  print &ui_table_row($text{'tikimanager_date'},        $info->{'date'}, 2);
  print &ui_table_row($text{'tikimanager_revision'},    $info->{'revision'}, 2);
  print &ui_table_row($text{'tikimanager_last_revision'},    $info->{'date_revision'}, 2);
  print &ui_table_end();
  print &ui_hr();

}

print &ui_tabs_start(\@tabs, "mode", $in{'mode'} ||
  ($is__actions ? "actions" : 
    (!$is__actions && $is__tiki_installed ? "importinstance" :
      (!$is__actions ? "installnew" : 
        ($is__cloneables ? "cloneables" : "cjobs") ) )), 1);

if (grep /actions/, @tabnames) {

  print &ui_tabs_start_tab("mode", "actions");
  print "<p>Overriding preferences and user access</p>\n";
  
  print &ui_table_start();
  print &ui_table_row("", "", 2);

  $info->{'branch'} =~ /(\d+)/;
  my $branch_num_info = $1;
  print &ui_table_row(
      ($branch_num_info > 20 || $info->{'branch'} eq 'master') ? "Edit tiki.ini.php" : "Edit tiki.ini",
    &ui_form_start("edit_manual.cgi?dom=$d->{'id'}")
    . &ui_submit("Edit", "edit", 0)
    . &ui_form_end(),
  2);
  print &ui_table_row("", "Override the preferences stored in the database as per <a href=\"https://doc.tiki.org/System-Configuration\""
      . " target=\"_blank\">"
      . " https://doc.tiki.org/System-Configuration"
      . '</a>', 2);
  print &ui_table_row("", "", 2);

  print &ui_table_row(
      "Instance Detect ",
      &ui_form_start("tiki-detect.cgi?dom=$d->{'id'}", 'post', undef, ("data=unbuffered-header-processor"))
          . &ui_submit("Detect", "detect", 0)
          . &ui_form_end(),
      2);
  print &ui_table_row("", "Detect PHP version and Tiki branch/tag", 2);
  print &ui_table_row("", "", 2);

  print &ui_table_row(
    "Unlock user",
    &ui_form_start("tiki-user-reset.cgi?dom=$d->{'id'}", 'post', undef, ("data=unbuffered-header-processor"))
    . &ui_textbox("user", "", undef, undef, undef, ("placeholder='Username'"))
    . &ui_submit("Unlock", "unlock", 0)
    . &ui_form_end(),
  2);
  print &ui_table_row("", "Unlock a user which was blocked after several failed login attempts.", 2);
  print &ui_table_row("", "", 2);

  print &ui_table_row(
    "Reset password",
    &ui_form_start("tiki-user-reset.cgi?dom=$d->{'id'}", 'post', undef, ("data=unbuffered-header-processor"))
    . &ui_textbox("user", "", undef, undef, undef, ("placeholder='Username'"))
    . &tikimanager_newpassword("password")
    . &ui_submit("Reset", "reset", 0)
    . &ui_form_end(),
  2);
  print &ui_table_row("", "Set a new password for the specified user.", 2);
  print &ui_table_end();
  print &ui_tabs_end_tab();
}

sub branch_label
{
    my $warning;
    $warning = "<span style='color: #cc5e00;'>$_[2]</span><br/> " if @_;
    my $label = 'Select branch';
    $label .= " (using $_[1] PHP version $_[0])" if @_;
    my $help_text = "Only compatible Tiki versions with the currently configured PHP version are shown. Click to change version.";
    return "$warning @{[&ui_text_wrap($label)]}".&ui_link('/virtual-server/edit_phpmode.cgi?dom='.$d->{'id'}, &ui_help($help_text), 'ui_link_help');
}

if (grep /instanceupdate/, @tabnames) {
  print &ui_tabs_start_tab("mode", "instanceupdate");
  # Update Instance Tiki
  print &ui_form_start("tiki-update.cgi?dom=$d->{'id'}", 'post', undef, ("data=unbuffered-header-processor"));
  print &ui_table_start("Update Instance",2);
  print &ui_table_row("", "Update to the latest revision of the chosen branch", 2);
  print &ui_table_row("", "", 2);
  print &ui_table_row(&hlink("Rebuild Index","instance_rebuildindex"), &ui_radio("rebuild_index", 'rebuild_open', [['rebuild_open', 'rebuild while site is kept open<br>'], ['rebuild_closed', 'rebuild while site is kept closed<br>'], ['skip', 'Skip<br>']]), 2);
  print &ui_table_row(&hlink("Stash","instance_stash"), &ui_radio("stash", 0, [[1, 'Yes<br>'], [0, 'No<br>']]), 2);
  print &ui_table_row("", "", 2);
  print &ui_table_row("", &ui_submit("Update", "update", 0), 2);
  print &ui_table_row("", "The latest updates of the $info->{'branch'} branch will be applied.", 2);
  print &ui_table_end();
  print &ui_form_end();
  # Upgrate Instance Tiki
  my ($upgradable_branches_ref, $upgradable_warn, $upgradable_phpversion, $upgradable_type, $upgradable_has_branches) = &tikimanager_tiki_list_possible_upgrades($d);
  my @upgradable_branches = @$upgradable_branches_ref;
  if ( @upgradable_branches ) {
    print &ui_form_start("tiki-upgrade.cgi?dom=$d->{'id'}", 'post', undef, ("data=unbuffered-header-processor"));
    print &ui_table_start("Upgrade Instance",2);
    print &ui_table_row("", "", 2);
    print &ui_table_row("", "Upgrade to the latest version of the chosen branch", 2);
    print &ui_table_row(&hlink("Rebuild Index","instance_rebuildindex"), &ui_radio("rebuild_index", 'rebuild_open', [['rebuild_open', 'rebuild while site is kept open<br>'], ['rebuild_closed', 'rebuild while site is kept closed<br>']]), 2);
    print &ui_table_row(&hlink("Stash","instance_stash"), &ui_radio("stash", 0, [[1, 'Yes<br>'], [0, 'No<br>']]), 2);
    print &ui_table_row("", "", 2);
    print &ui_table_row(&branch_label($upgradable_phpversion, $upgradable_type, $upgradable_warn),
      &ui_select("branch", $latest_branch, 
        [tikimanager_tiki_process_branches(@upgradable_branches)]) .
          &ui_submit("Upgrade", "upgrade", 0), 2);
    print &ui_table_row("", "", 2);
    print &ui_table_row("", "The selected version of Tiki will be installed in place of the old one, and the database schema will be converted. You can't downgrade later, so make sure to have a backup.", 2);
    print &ui_table_row("", "", 2);
    print &ui_table_end();
    print &ui_form_end();
  }
  print &ui_tabs_end_tab();
}

if (grep /importinstance/, @tabnames) {
  print &ui_tabs_start_tab("mode", "importinstance");
  print "<p>Permit management of an already existing Tiki instance</p>\n";
  print &ui_form_start("tiki-import.cgi?dom=$d->{'id'}", 'post', undef, ("data=unbuffered-header-processor"));
  print &ui_table_start();
  print &ui_table_row("", "", 2);
  print &ui_table_row("Tiki found at:", "'$d->{'public_html_path'}'", 2);
  print &ui_table_row("", "", 2);
  print &ui_table_row("", "Do you want to import it?&nbsp;&nbsp;" . ui_submit("Import", "import", 0), 2);
  print &ui_table_row("", "", 2);
  print &ui_table_end();
  print &ui_form_end();
  print &ui_tabs_end_tab();
}

if (grep /installnew/, @tabnames) {
  print &ui_tabs_start_tab("mode", "installnew");
  print "<p>Install a new Tiki instance and set some basic preferences. The admin user has full permissions in Tiki. You can use it to create other users and add them to the Admins group.</p>\n";
  print &ui_form_start("tiki-install.cgi?dom=$d->{'id'}", 'post', undef, ("data=unbuffered-header-processor"));
  print &ui_table_start();
  print &ui_table_row("", "", 2);
  print &ui_table_row("Sender email", &ui_textbox('sender_email', $d->{'emailto'}, 48), 2);
  print &ui_table_row("", "Email to be displayed as 'sender' or 'from' for recipient users receiving emails from this Tiki", 2);
  print &ui_table_row("", "", 2);
  print &ui_table_row("Email of the admin user", &ui_textbox('admin_email', $d->{'emailto'}, 48), 2);
  print &ui_table_row("", "Some administrative alerts may be sent to this email. This email can also be used to reset the admin password.", 2);
  print &ui_table_row("", "", 2);
  print &ui_table_row("Password of the admin user", &tikimanager_newpassword('password'), 2);
  print &ui_table_row("", "Copy or change the password above. You will not see it again, but you can reset it later.", 2);
  print &ui_table_row("", "", 2);
  print &ui_table_row(&branch_label($branches_phpversion, $branches_type, $branches_warn),
    &ui_select("branch", $latest_branch,
      [tikimanager_tiki_process_branches(@tiki_branches)]) .
        &ui_submit($text{'index_install'}, "save", 0), 2);
  print &ui_table_row("Warning", &text("index_install_tiki_warning", "$d->{'public_html_path'}"), 2);
  print &ui_table_end();
  print &ui_form_end();
  print &ui_tabs_end_tab();
}

if (grep /cloneables/, @tabnames) {
  #
  # Clone
  #
  print &ui_tabs_start_tab("mode", "cloneables");
  if ($count_cloneables > 0) {
    print "<p>Make a copy of a Tiki instance, which is useful for testing. Please see https://dev.tiki.org/Divergent-Preferences-in-Staging-Development-Production</p>\n";
    print &ui_form_start("tiki-clone.cgi?dom=$d->{'id'}", 'post', undef, ("data=unbuffered-header-processor"));
    print &ui_table_start("Clone from another Tiki instance", "width=100%", 2);
    print &ui_table_row("", "", 2);
    print &ui_table_row("", &text("index_clone_tiki_message"), 2);
    print &ui_table_row("", "", 2);
    print &ui_table_row("Select source", &ui_select("source", undef,
      \@cloneables) . ui_submit("Clone", "save", 0), 2);
    print &ui_table_row("Warning", &text("index_install_tiki_warning", "$d->{'public_html_path'}"), 2);
    print &ui_table_end();
    print &ui_form_end();
  }

  #
  # Clone and upgrade
  #
  my ($clonable_branches_ref, $clonable_warn, $clonable_phpversion, $clonable_type, $clonable_has_branches) = &tikimanager_get_installable_branches($d);
  my @clonable_branches = @$clonable_branches_ref;
  print &ui_form_start("tiki-cloneandupgrade.cgi?dom=$d->{'id'}", 'post', undef, ("data=unbuffered-header-processor"));
  print &ui_table_start("Clone from another Tiki instance and then upgrade", "width=100%", 2);
  print &ui_table_row("", "", 2);
  print &ui_table_row("", &text("index_clone_tiki_message"), 2);
  print &ui_table_row("", "", 2);
  print &ui_table_row("Select source",
    &ui_select("source", undef, \@upgradeables,
      undef, undef, undef, undef, ("id=cloneandupgrade-source-instance")), 2);
  print &ui_table_row("", "", 2);
  print &ui_table_row(&branch_label($clonable_phpversion, $clonable_type, $clonable_warn), 
    &ui_select("branch", $latest_branch,
      [tikimanager_tiki_process_branches(@clonable_branches)],
        undef, undef, undef, undef, ("id=cloneandupgrade-target-branch")) .
          ui_submit("Clone and Upgrade", "cloneandupgrade", 0), 2);
  print &ui_table_row("Warning", &text("index_install_tiki_warning", "$d->{'public_html_path'}"), 2);
  print &ui_table_end();
  print &ui_form_end();  
  print &ui_tabs_end_tab();
}

if (grep /cjobs/, @tabnames) {
  if (@jobs) {
    print &ui_tabs_start_tab("mode", "cjobs");
    print "<p>Scheduled Cron Jobs for this instance, essential for https://doc.tiki.org/Scheduler</p>\n";
    print &ui_columns_start([
      'Active',
      'Time',
      'Command',
    ]);
    my $idx = 0;
    foreach my $job (@jobs) {
      if ($job->{'user'} eq $d->{'user'}) {
          my @cols;
          push(@cols, $job->{'active'} ? $text{'yes'} : "<font color=#ff0000>$text{'no'}</font>");
          push(@cols, "<tt>$job->{'mins'} $job->{'hours'} $job->{'days'} $job->{'months'} $job->{'weekdays'}</tt>");
          push(@cols, &ui_link("/cron/edit_cron.cgi?idx=" . $idx, $job->{'command'}));
          print &ui_columns_row(\@cols);
      }
      $idx += 1;
    }
    print &ui_columns_end();
    print &ui_tabs_end_tab();
  }
}

print &ui_tabs_end(1);

print <<EOF
<script type="text/javascript">
  var options;
  function tooglePassword (id) {
    	let newinput = document.getElementById(id);
        if (newinput.type != 'text') {
        	newinput.type = 'text';
        } else {
        	newinput.type = 'password';
        }
    }
	function generatePassword (id) {
    	let newinput = document.getElementById(id);
        newinput.value = makeRandomString(10);
    }
    function makeRandomString(length) {
    	let result = '';
    	const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    	const charactersLength = characters.length;
    	for (let i = 0; i < length; i++) {
        	result += characters.charAt(Math.floor(Math.random() * charactersLength));
    	}
    	return result;
	}
  jQuery(function() {
    function getOptionValues(selector) {
      return jQuery(selector).find('option').map(function() {
        return jQuery(this).val();
      }).get();
    }

    function updateTargetBranchOptions() {
      // Clear existing options
      jQuery("#cloneandupgrade-target-branch").empty();

      var sourceInstance = jQuery("#cloneandupgrade-source-instance option:selected").text();
      var version = sourceInstance.split("(")[1].slice(0, -1);
      version = jQuery.trim(version);

      if (version === "master") {
        return;
      } 

      jQuery.each(window.options, function(index, value) {
        jQuery("#cloneandupgrade-target-branch").append('<option value="' + value + '">' + value + '</option>');
      });
    }

    // Get all option values on document load
    window.options = getOptionValues('#cloneandupgrade-target-branch');
    // Update the target branch options on document load
    updateTargetBranchOptions();
    // Bind the function to the select element's change event
    jQuery('#cloneandupgrade-source-instance').on('change', function() {
      updateTargetBranchOptions();
    });
  });

  jQuery("form[data=unbuffered-header-processor]").on("submit", function(evt) {
    evt.preventDefault();
    unbuffered_header_processor(evt, 1);
    return false;
  });
</script>
EOF
;

# If the instance is created setup tiki config files.
my $instance_cache = tikimanager_read_cache('instance-list');
my @instance_list = @{$instance_cache->[0] || []};
my @valid_domain_info = @{$instance_cache->[1] || []};
# scan all domains available
if (!@instance_list) {
  foreach my $domain (grep { &virtual_server::domain_has_website($_) } &virtual_server::list_domains()) {
    # get the instance info from the domain
    my ($instance_infos) = &tikimanager_tiki_info($domain);

    # if the instance is created setup tiki config files.
    if ($instance_infos && $instance_infos->{'instance_id'}) {
      if (-d "$domain->{'public_html_path'}") {
        &tikimanager_setup_tikiconfig_file($domain);
      }
      push(@instance_list, $instance_infos);
      push(@valid_domain_info, {
        public_html_path => $domain->{'public_html_path'},
        id => $domain->{'id'},
        instance_id => $instance_infos->{'instance_id'},
      });
    }
  }
  @instance_list = sort {
    $a->{'instance_id'} <=> $b->{'instance_id'} } @instance_list;
  @valid_domain_info = sort {
    $a->{'instance_id'} <=> $b->{'instance_id'} } @valid_domain_info;
}
# Cache the instance list
tikimanager_save_cache([\@instance_list, \@valid_domain_info], 'instance-list');
# As root list all instances created
my $count_instances = @instance_list;
if (&virtual_server::master_admin() && $count_instances > 0) {
  my $col = &ui_columns_start([
      'ID',
      'Name',
      'Branch',
      'Last update',
      'Date last revision',
      'Git revision',
      ''
  ]);
  for my $i (0..$#instance_list) {
    my $instance = $instance_list[$i];
    my $dom_infos = $valid_domain_info[$i];
    if ($instance->{'webroot'} eq $dom_infos->{'public_html_path'}) {
        my @cols = (
            $instance->{'instance_id'},
            $instance->{'name'},
            $instance->{'branch'},
            $instance->{'date'},
            $instance->{'revision'},
            $instance->{'date_revision'},
            &ui_link("@{[&get_webprefix()]}/$module_name/index.cgi".
                      "?dom=$dom_infos->{'id'}", "Show")
        );
        $col .= &ui_columns_row(\@cols);
    }
  }
  $col .= &ui_columns_end();
  $col .= &ui_form_start("reset-tiki-cache.cgi?dom=$d->{'id'}")
          . &ui_hidden("type", "instance-list")
          . &ui_form_end([ [ "clear", $text{'index_clear_cache'} ] ]);
  print &ui_hr();
  print &ui_hidden_table_start('Instances', "width=100%", 2, "main", 1);
  print &ui_table_row(undef, $col);
  print &ui_hidden_table_end('main');
}

# Allow to set Tiki Manager FROM_EMAIL_ADDRESS
if (&virtual_server::master_admin()) {
  my %env_info = &tikimanager_env_info();

  print &ui_hr();
  print &ui_form_start("tiki-manager-email.cgi?dom=$d->{'id'}", 'post', undef, ("data=unbuffered-header-processor"));
  print &ui_hidden_table_start('Tiki Manager Email Configuration', "width=100%", 1, "tiki-manager-email", 0);
  print &ui_table_row(
    "Email address to use as 'from' in emails sent.",
    &ui_textbox('from_email_address', $env_info{'FROM_EMAIL_ADDRESS'}, 48) . "&nbsp;" .
    &ui_submit("Save", "save", 0)
  );
  print &ui_hidden_table_end('tiki-manager-email');
}

# Make sure the left menu is showing this domain
if (defined(&theme_select_domain)) {
	&theme_select_domain($d);
}

&ui_print_footer(
  $d ? &virtual_server::domain_footer_link($d) : ( ),
  "@{[&get_webprefix()]}/virtual-server/",
  $virtual_server::text{'index_return'}
);
