#!/usr/bin/perl
use strict;
use warnings;

our (%access, %text, %in, %config);
our ($module_name);

require './virtualmin-tikimanager-lib.pl';
&ReadParse();

my $d;
if ($in{'dom'}) {
  $d = &virtual_server::get_domain($in{'dom'});
}
$d || &error($virtual_server::text{'edit_egone'});

my $config = &tikimanager_tiki_get_config($d, 2);
my ($file, $is_new_branch) = &tikimanager_get_instance_config_file($d);
my $php_code = &tikimanager_get_ini_php_code();
$file = $config->{'system_configuration_file'}
    if (ref($config) eq 'HASH' && -e $config->{'system_configuration_file'});
my $data = "<?php\n";
$data = &virtual_server::read_file_contents_as_domain_user($d, $file)
  if (-r "$file");
my $dir = $file;
$dir =~ s/\/[^\/]*$//;
&virtual_server::make_dir_as_domain_user($d, $dir) if (!-d $dir);
&ui_print_header("<tt>$file</tt>", $text{'tikimanager_manual_title'}, "");
print $text{'tikimanager_manual_desc'},"<p>\n";
print &ui_form_start("save_manual.cgi", "form-data");
print &ui_hidden("dom", $d->{'id'}), "\n";
print &ui_hidden("file", $file), "\n";
print &ui_textarea("data", $data, 20, 80);
print &ui_form_end([ [ "save", $text{'save'} ] ]);
&ui_print_footer(
  "index.cgi?dom=$in{'dom'}", $text{'index_the_information_page'},
  &virtual_server::domain_footer_link($d),
);
