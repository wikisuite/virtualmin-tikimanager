#!/usr/bin/perl
use strict;
use warnings;
use Email::Valid;

our (%access, %text, %in);
our $module_name;

require './virtualmin-tikimanager-lib.pl';
&ReadParse();

# check if user can access the page
&can_domain($in{'dom'}) || &error($text{'contact_ecannot'});

my $d;
if ($in{'dom'}) {
  $d = &virtual_server::get_domain($in{'dom'});
}

if (! $d) {
  &error("Domain not found.");
}

if (!&virtual_server::master_admin()) {
  &error("Operation not allowed.");
}

# Page title, must be first UI thing
&ui_print_header(
  'from ' . '<a href="https://' . $d->{'dom'} .'" target="_blank">https://' . $d->{'dom'} .'</a>',
  "Updating source email address for tiki manager", "", undef, 1, 1
);

&$virtual_server::first_print("Updating Tiki Manager's FROM_EMAIL_ADDRESS ...");
print '<pre style="white-space: pre-wrap; background-color: black; color: white;">' . "\n";

my %manager = &tikimanager_info();
my $env_file = "$manager{'home'}/.env";
my $email = $in{'from_email_address'} || '';

&update_tiki_manager_email($env_file, $email);

print '</pre>' . "\n";
&$virtual_server::second_print(".. done");

&ui_print_footer(
  $d ? &virtual_server::domain_footer_link($d) : ( ),
  "index.cgi?dom=$in{'dom'}",
  $text{'index_the_information_page'}
);

sub update_tiki_manager_email {
  my ($env_file, $email) = @_;

  if ($email) {
    unless (Email::Valid->address($email)) {
        print "The email used ($email) is not a valid email address...";
        return;
    }
  }

  my $env_lines_ref = &tikimanager_env_file_read($env_file);

  unless (defined($env_lines_ref)) {
    print "There was a problem opening the env file ($env_file) for writing...";
    return;
  }

  print "Updating the env file ($env_file) with email ...";
  tikimanager_env_file_update($env_file, $env_lines_ref, $email);
}
