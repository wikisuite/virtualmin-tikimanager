#!/usr/bin/perl
use strict;
use warnings;
our (%text, %in, %config);
our ($module_name);

require './virtualmin-tikimanager-lib.pl';
&error_setup($text{'tikimanager_manual_err'});
&ReadParseMime();

# Get domain or show error
my $d;
if ($in{'dom'}) {
  $d = &virtual_server::get_domain($in{'dom'});
}
$d || &error($virtual_server::text{'edit_egone'});
&virtual_server::can_config_domain($d) ||
    &error($virtual_server::text{'edit_ecannot'});
$in{'data'} =~ s/\r//g;
$in{'data'} =~ /\S/ || &error($text{'tikimanager_manual_edata'});
&virtual_server::write_as_domain_user($d,
        sub { &write_file_contents($in{'file'}, $in{'data'}) });
&redirect("index.cgi?dom=$in{'dom'}");
